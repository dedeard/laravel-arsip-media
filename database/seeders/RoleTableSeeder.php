<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            'mengelola peran',
            // 'mengelola izin',

            'baca pengguna',
            'buat pengguna',
            'edit pengguna',
            'hapus pengguna',
            'edit peran pengguna',

            'baca album',
            'buat album',
            'edit album',
            'hapus album',
            'share file'
        ];

        foreach($permissions as $name) {
            Permission::create(['name' => $name]);
        }

        $superAdminRole = Role::create(['name' => 'Super Admin']);
        $superadmin = User::where('email', 'super@admin.com')->first();
        $superadmin->syncRoles($superAdminRole);

        $adminRole = Role::create(['name' => 'Admin']);
        $adminRole->syncPermissions($permissions);
    }
}
