// import PhotoSwipe from 'photoswipe/dist/photoswipe'
// import PhotoSwipeUI from 'photoswipe/dist/photoswipe-ui-default'
import Clipboard from 'v-clipboard'

// import createPreviewDirective from 'vue-photoswipe-directive'
import { openSidebar, closeSidebar, toggleSidebar } from './directives/Sidebar'
import DeleteConfirm from './directives/delete-confirm'

import AppSidebar from './components/AppSidebar'
import AppAlert from './components/AppAlert'
import MediaList from './components/MediaList'
import MediaUploader from './components/MediaUploader'

Vue.use(Clipboard)

// Vue.directive('preview', createPreviewDirective(null, PhotoSwipe, PhotoSwipeUI))
Vue.directive('toggle-sidebar', toggleSidebar)
Vue.directive('open-sidebar', openSidebar)
Vue.directive('close-sidebar', closeSidebar)
Vue.directive('delete-confirm', DeleteConfirm)

Vue.component('AppSidebar', AppSidebar)
Vue.component('AppAlert', AppAlert)
Vue.component('MediaUploader', MediaUploader)
Vue.component('MediaList', MediaList)

new Vue({
  el: '#app'
})
