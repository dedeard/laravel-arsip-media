export default {
  data() {
    return {
      open: true
    }
  },
  methods: {
    close() {
      this.open = false
    }
  },
}
