@extends('layouts.app')
@section('style:after')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection
@section('script:after')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

@endsection
@section('content')
  <div class="container-fluid">
    <div class="panel panel-bordered">
      <div class="panel-heading">
        <h3 class="panel-title">{{ $album->title }}</h3>
        <div class="panel-actions">
          @can('edit album')
          <a href="{{ route('albums.edit', $album->id) }}" class="btn btn-sm btn-primary">Edit</a>
          @endcan
          @can('share file')
          <a href="{{ route('albums.share.album', $album->id) }}" class="btn btn-sm btn-primary">Share</a>
          @endcan
          @can('hapus album')
          <button class="btn btn-danger btn-sm" v-delete-confirm:form-delete-album>Hapus</button>
          <form action="{{ route('albums.destroy', $album->id) }}" method="post" id="form-delete-album">
            @csrf
            @method('delete')
          </form>
          @endcan
        </div>
      </div>
      <div class="panel-body">
        <p class="m-0 small font-weight-bold">Terakhir diubah {{ $album->updated_at->isoFormat('D MMMM YYYY') }}</p>
        <p>{{ $album->description }}</p>
      </div>
    </div>
    @can('edit album')
    <media-uploader class="mb-3" upload-url="{{ route('albums.upload', $album->id) }}" ></media-uploader>
    @endcan
    <div class="row">
      @foreach ($album->media as $media)
        <div class="col-lg-2 col-md-3 col-sm-4">
          <media-list size="{{ $media->humanSize() }}" thumb="/storage/thumb/{{ $media->name }}.jpeg" media="{{ route('albums.file', $media->id) }}" original_name="{{ $media->original_name }}" @if($media->type == 'video') video @endif>
            <a href="{{ route('albums.file', $media->id) }}" download class="btn btn-primary btn-xs btn-block">Download</a>
            @can('share file')
              <a href="{{ route('albums.share.file', $media->id) }}" class="btn btn-primary btn-xs btn-block">Share</a>
            @endif
            @can('edit album')
              <button class="btn btn-danger btn-xs btn-block" v-delete-confirm:form-delete-file-{{ $media->id }}>Hapus</button>
              <form action="{{ route('albums.destroy.file', [$album->id, $media->id]) }}" method="post" id="form-delete-file-{{ $media->id }}">
                @csrf
                @method('delete')
              </form>
            @endcan
          </media-list>
        </div>
      @endforeach
    </div>
  </div>
@endsection
