@extends('layouts.app')

@section('script:after')
@endsection

@section('content')
  <div class="container-fluid">
    <div class="panel panel-bordered">
      <div class="panel-heading">
        <h3 class="panel-title">Share {{ $type }}</h3>
        <div class="panel-actions">
          <a href="#" onclick="history.back()" class="btn btn-danger btn-sm">Kembali</a>
        </div>
      </div>
      <div class="panel-body">
        <h4>@if($type=='file') {{ $media->original_name }} @else {{ $album->title }} @endif</h4>
        <form method="post" action="{{ route('albums.share.'. $type, $type == 'file' ? $media->id : $album->id) }}">
          @csrf
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama..." value="{{ old('name') }}">
            @error('name')
              <span class="invalid-feedback">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group">
            <label for="hours">Waktu Kadaluarsa (jam)</label>
            <input type="number" class="form-control @error('hours') is-invalid @enderror" name="hours" placeholder="Waktu Kadaluarsa (jam)..." value="{{ old('hours') }}">
            @error('hours')
              <span class="invalid-feedback">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Buat link</button>
          </div>
        </form>
      </div>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>nama</th>
              <th>url</th>
              <th>Kadaluarsa</th>
              <th class="text-center" style="width: 140px;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @php
              if($type == 'file'){
                $shares = $media->shares;
              } else {
                $shares = $album->shares;
              }
            @endphp
            @foreach($shares as $share)
            <tr>
              <td class="align-middle">{{ $share->name }}</td>
              <td class="align-middle"><a href="{{ route('share.' . $type, $share->token) }}" target="_blank">{{ route('share.' . $type, $share->token) }}</a></td>
              <td class="align-middle">
                @if ($share->expired_at > now())
                {{ $share->expired_at->diffForHumans() }}
                @elseif($share->expired_at === null)
                Tak berwaktu
                @else
                Kadaluarsa
                @endif
              </td>
              <td class="text-center py-0 align-middle">
                <button href="#" class="btn btn-outline-default btn-xs" v-clipboard="'{{ route('share.' . $type, $share->token) }}'">Copy</button>
                <button class="btn btn-outline-danger btn-xs" v-delete-confirm:form-delete-share-{{ $share->id }}>Hapus</button>
                <form action="{{ route('albums.share.' . $type, $share->id) }}" method="post" id="form-delete-share-{{ $share->id }}">
                  @csrf
                  @method('delete')
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
