@extends('layouts.app')



@section('content')
  <div class="container-fluid">
    <div class="panel panel-bordered">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar Album</h3>
        <div class="panel-actions">
          @can('buat album')
            <a href="{{ route('albums.create') }}" class="btn btn-sm btn-primary">Buat Album</a>
          @endcan
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <form method="get" class="input-group">
          <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request()->input('search') }}">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
            </span>
          </form>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Judul</th>
              <th>Total file</th>
              <th>Ukuran</th>
              <th>Terakhir diubah</th>
              <th class="text-center" style="width: 140px;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($albums as $album)
            <tr ondblclick="location.href = '{{ route('albums.show', $album->id) }}'">
              <td class="align-middle">{{ $album->title }}</td>
              <td class="align-middle">{{ $album->media_count }}</td>
              <td class="align-middle">{{ $album->humanSize() }}</td>
              <td  class="align-middle">{{ $album->updated_at->isoFormat('D MMMM YYYY') }}</td>
              <td class="text-center py-0 align-middle">
                <a href="{{ route('albums.show', $album->id) }}" class="btn btn-outline-default btn-sm btn-icon" title="Edit Pengguna"><i class="wb-eye"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="panel-footer">
        {{ $albums->links() }}
      </div>
    </div>
  </div>
@endsection
