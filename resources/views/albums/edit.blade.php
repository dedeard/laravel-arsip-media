@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="panel panel-bordered">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Album</h3>
      <div class="panel-actions">
        <a href="#" onclick="history.back()" class="btn btn-danger btn-sm">Kembali</a>
      </div>
    </div>
    <div class="panel-body">
      <form action="{{ route('albums.update', $album->id) }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="title">Judul</label>
          <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Judul..." value="{{ $album->title }}">
          @error('title')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <label for="description">Deskripsi</label>
          <textarea class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Deskripsi...">{{ $album->description }}</textarea>
          @error('description')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Edit Album</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
