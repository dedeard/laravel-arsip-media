<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <x-head />

  <!-- css -->
  @yield('style:before')
  <link rel="stylesheet" href="{{ mix('css/theme.css') }}">
  <link rel="stylesheet" href="{{ mix('css/auth.css') }}">
  @yield('style:after')
</head>

<body>
  <div class="container my-auto pt-5 auth-layout">
    <div class="row">
      <div class="col-lg-5 col-md-8 mx-auto">
        <div class="panel">
          <div class="panel-body bg-grey-900">
            <div class="text-center">
              <h2 class="text-light m-0 font-weight-bold">ARSIP KEGIATAN</h2>
              <h4 class="text-light m-0 font-weight-light">PROVINSI SULAWESI SELATAN</h4>
            </div>
          </div>
          <div class="panel-body pt-2">
            <div class="text-center">
              <img src="{{ asset('img/brand.png') }}" alt="Logo gubernur provinsi sulawesi selatan" width="160px">
            </div>
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>
