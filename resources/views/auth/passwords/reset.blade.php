@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('password.update') }}">
  @csrf
  <h2 class="text-center mb-3 font-weight-bold text-uppercase">{{ __('Reset Password') }}</h2>
  <input type="hidden" name="token" value="{{ $token }}">
  <div class="form-group">
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
    @error('email')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">
    @error('password')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
    @error('password')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary font-weight-bold btn-block btn--float">{{ __('Reset Password') }}</button>
  </div>
  <div class="form-group mb-4 text-center">
    <a class="btn btn-link" href="{{ route('login') }}">
      {{ __('Login') }}
    </a>
  </div>
</form>
@endsection
