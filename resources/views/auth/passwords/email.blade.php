@extends('layouts.auth')

@section('content')

@if (session('status'))
<div class="alert alert-success mt-3" role="alert">
  {{ session('status') }}
</div>
@endif

<form method="POST" action="{{ route('password.email') }}">
  @csrf
  <h2 class="text-center mb-3 font-weight-bold text-uppercase">{{ __('Reset Password') }}</h2>
  <div class="form-group">
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
    @error('email')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary font-weight-bold btn-block btn--float">{{ __('Send Password Reset Link') }}</button>
  </div>
  <div class="form-group mb-4 text-center">
    <a class="btn btn-link" href="{{ route('login') }}">
      {{ __('Login') }}
    </a>
  </div>
</form>
@endsection
