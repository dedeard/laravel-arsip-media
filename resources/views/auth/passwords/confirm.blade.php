@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('password.confirm') }}">
  @csrf
  <h2 class="text-center font-weight-bold text-uppercase">{{ __('Confirm Password') }}</h2>
  <p class="text-center lead mb-3">{{ __('Please confirm your password before continuing.') }}</p>
  <div class="form-group">
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
    @error('password')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary font-weight-bold btn-block btn--float">{{ __('Confirm Password') }}</button>
  </div>
  <div class="form-group mb-4 text-center">
    <a class="btn btn-link" href="{{ route('password.request') }}">
      {{ __('Forgot Your Password?') }}
    </a>
  </div>
</form>
@endsection
