@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('login') }}">
  @csrf
  <h2 class="text-center mb-3 font-weight-bold text-uppercase">{{ __('Login') }}</h2>
  <div class="form-group">
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
    @error('email')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
    @error('password')
    <span class="invalid-feedback" role="alert">
      <strong>{{ $message }}</strong>
    </span>
    @enderror
  </div>
  <div class="form-group">
    <div class="checkbox-custom checkbox-primary">
      <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }} name="remember">
      <label for="remember">{{ __('Remember Me') }}</label>
    </div>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary font-weight-bold btn-block btn--float">{{ __('Login') }}</button>
  </div>
  <div class="form-group mb-4 text-center">
    <a class="btn btn-link" href="{{ route('password.request') }}">
      {{ __('Forgot Your Password?') }}
    </a>
  </div>
</form>
@endsection
