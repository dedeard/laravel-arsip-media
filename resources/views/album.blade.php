@extends('layouts.blank')
@section('style:after')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection
@section('script:after')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endsection
@section('content')
  <div class="container-fluid">
    <div class="panel panel-bordered">
      <div class="panel-heading">
        <h3 class="panel-title">{{ $album->title }}</h3>
      </div>
      <div class="panel-body">
        <p class="m-0 small font-weight-bold">Terakhir diubah {{ $album->updated_at->isoFormat('D MMMM YYYY') }}</p>
        <p>{{ $album->description }}</p>
      </div>
    </div>
    <div class="row">
      @foreach ($album->media as $media)
        <div class="col-lg-2 col-md-3 col-sm-4">
          <media-list size="{{ $media->humanSize() }}" thumb="/storage/thumb/{{ $media->name }}.jpeg" media="{{ route('share.album.file', [$token, $media->id]) }}" original_name="{{ $media->original_name }}" @if($media->type == 'video') video @endif>
            <a href="{{ route('share.album.file', [$token, $media->id]) }}" download class="btn btn-primary btn-xs btn-block">Download</a>
          </media-list>
        </div>
      @endforeach
    </div>
  </div>
@endsection
