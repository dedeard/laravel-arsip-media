<div class="app-header">
  <div class="app-header__wrapper">
    <div class="app-header__left">
      <div class="container-fluid">
        <div class="wrapper">
          <a href="{{ route('dashboard') }}" class="brand">
            <span class="first">ARSIP KEGIATAN</span>
            <span class="last">PROVINSI SULAWESI SELATAN</span>
          </a>
        </div>
      </div>
    </div>
    <div class="app-header__right">
      <div class="container-fluid">
        <div class="wrapper">
          <div class="quick-actions">
            <ul class="nav">
            @can('buat album')
              <li class="nav-item">
                <a class="active nav-link" href="{{ route('albums.create') }}">Buat Album Baru</a>
              </li>
            @endcan
            </ul>
          </div>
          <div class="user-actions">
            <a href="javascript:;" class="btn btn-action sidebar-toggle" v-open-sidebar>
              <i class="wb-grid-4"></i>
            </a>
            <a href="{{ route('profile.logout') }}" title="Keluar" class="btn btn-action text-danger">
              <i class="wb-power"></i>
            </a>

            {{-- <a href="javascript:;" class="btn btn-action">
              <i class="wb-bell"></i>
              <span class="label">5</span>
            </a> --}}
            <a href="{{ route('profile.index') }}" class="avatar img-bordered bg-white" ><img src="{{ Auth::user()->avatar_url }}" class="img-fluid" /></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
