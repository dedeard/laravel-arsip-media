@php
$linkGroups = [
  [
    [
      'name' => 'Dashboard',
      'route' => 'dashboard',
      'is' => 'dashboard',
      'icon' => 'wb-globe',
    ],
    [
      'name' => 'Album',
      'route' => 'albums.index',
      'is' => 'albums*',
      'icon' => 'wb-gallery',
      'can' => 'baca album',
    ],
  // ],
  // [
    [
      'name' => 'Pengguna',
      'route' => 'users.index',
      'is' => 'users*',
      'icon' => 'wb-users',
      'can' => 'baca pengguna',
    ],
    [
      'name' => 'Peran',
      'route' => 'roles.index',
      'is' => 'roles*',
      'icon' => 'wb-lock',
      'can' => 'mengelola peran',
    ],
    // [
    //   'name' => 'Izin',
    //   'route' => 'permissions.index',
    //   'is' => 'permissions*',
    //   'icon' => 'wb-lock',
    //   'can' => 'mengelola izin',
    // ],
  // ],
  // [
    [
      'name' => 'Profile',
      'route' => 'profile.index',
      'is' => 'profile.index',
      'icon' => 'wb-user',
    ],
    [
      'name' => 'Logout',
      'route' => 'profile.logout',
      'is' => 'profile.logout',
      'icon' => 'wb-power text-danger',
    ],
  ],
];
@endphp

<section class="page-aside-section">
  @foreach($linkGroups as $linkGroup)
    @if(count($linkGroup))
      <div class="list-group">
        @foreach($linkGroup as $l)
          @if(isset($l['can']))
            @can($l['can'])
              @if(empty($l['not']))
                <a class="list-group-item @if(Route::is($l['is'])) active @endif" href="{{ route($l['route']) }}">
                  <i class="icon {{ $l['icon'] }}"></i>
                  {{ $l['name'] }}
                </a>
              @else
                <a class="list-group-item @if(Route::is($l['is']) && !Route::is($l['not'])) active @endif" href="{{ route($l['route']) }}">
                  <i class="icon {{ $l['icon'] }}"></i>
                  {{ $l['name'] }}
                </a>
              @endif
            @endcan
          @else
          <a class="list-group-item @if(Route::is($l['is'])) active @endif" href="{{ route($l['route']) }}">
            <i class="icon {{ $l['icon'] }}"></i>
            {{ $l['name'] }}
          </a>
          @endif
        @endforeach
      </div>
    @endif
  @endforeach
</section>
