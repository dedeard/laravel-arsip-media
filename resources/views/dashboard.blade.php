@extends('layouts.app')

@section('content')
<div class="dashboard-header mb-3 mt--10" style="background-image: url({{ asset('img/kgpss.jpg') }})">
  <div class="text-center blue-grey-800 py-80">
    <div class="font-size-40 m-0 text-white text-capitalize">Halo, {{ Auth::user()->name }}</div>
    <a href="{{ route('profile.logout') }}" class="btn btn-danger px-30">KELUAR</a>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-lg-6">
      <div class="card card-block bg-blue-600">
        <div class="counter counter-lg counter-inverse">
          <div class="counter-number-group">
            <span class="counter-number">
              {{ \App\Models\Album::count() }}
            </span>
          </div>
          <div class="counter-label text-uppercase font-size-16">Total Album</div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="card card-block bg-blue-600">
        <div class="counter counter-lg counter-inverse">
          <div class="counter-number-group">
            <span class="counter-number">
              {{ \App\Models\Media::count() }}
            </span>
          </div>
          <div class="counter-label text-uppercase font-size-16">Total File</div>
        </div>
      </div>
    </div>

    <div class="col">
      <div class="card card-block">
        <div class="counter counter-lg">
          <div class="counter-number-group">
            <span class="counter-number">
              {{ \App\Models\Media::select(\DB::raw('SUM(size) as size'))->first()->humanSize() }}
            </span>
          </div>
          <div class="counter-label text-uppercase font-size-16">Total Ukuran File</div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
