@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="panel panel-bordered">
    <div class="panel-heading">
      <h3 class="panel-title">Profile Saya</h3>
      <div class="panel-actions">
        <a href="{{ route('profile.logout') }}" class="btn btn-danger btn-sm">Logout</a>
      </div>
    </div>
    <div class="panel-body">
      <form action="{{ route('profile.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama..." value="{{ $user->name }}">
          @error('name')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email..." value="{{ $user->email }}">
          @error('email')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <label for="new_password">Password baru</label>
          <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" placeholder="Password baru...">
          @error('new_password')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Edit Profile</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
