@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="panel panel-bordered">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar Pengguna</h3>
        <div class="panel-actions">
          @can('buat pengguna')
            <a href="{{ route('users.create') }}" class="btn btn-sm btn-primary">Buat Pengguna</a>
          @endcan
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <form method="get" class="input-group">
          <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request()->input('search') }}">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
            </span>
          </form>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Email</th>
              <th>Peran</th>
              <th class="text-center" style="width: 140px;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td class="align-middle">{{ $user->name }}</td>
              <td  class="align-middle">{{ $user->email }}</td>
              <td  class="align-middle">{{ empty($user->roles[0]) ? '' : $user->roles[0]->name }}</td>
              <td class="text-center py-0 align-middle">
                @if(!$user->hasRole('Super Admin') || Auth::user()->hasRole('Super Admin'))
                  @can('edit pengguna')
                  <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-default btn-sm btn-icon" title="Edit Pengguna"><i class="wb-pencil"></i></a>
                  @endcan
                  @if(!$user->hasRole('Super Admin'))
                    @can('hapus pengguna')
                    <button class="btn btn-danger btn-sm btn-icon" title="Hapus Pengguna" v-delete-confirm:form-delete-user-{{ $user->id }}>
                      <i class="wb-trash"></i>
                    </button>
                    <form action="{{ route('users.destroy', $user->id) }}" method="post" id="form-delete-user-{{ $user->id }}">
                      @csrf
                      @method('delete')
                    </form>
                    @endcan
                  @endif
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="panel-footer">
        {{ $users->links() }}
      </div>
    </div>
  </div>
@endsection
