@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="panel panel-bordered">
    <div class="panel-heading">
      <h3 class="panel-title">Buat Pengguna</h3>
      <div class="panel-actions">
        <a href="#" onclick="history.back()" class="btn btn-danger btn-sm">Kembali</a>
      </div>
    </div>
    <div class="panel-body">
      <form action="{{ route('users.store') }}" method="POST">
        @csrf
        @can('edit peran pengguna')
        <div class="form-group">
          <label for="role">Peran</label>
          <select class="form-control @error('role') is-invalid @enderror" name="role" id="role">
            @foreach($roles as $role)
              <option value="{{ $role->id }}" @if(old('role') == $role->id) selected @endif>{{ $role->name }}</option>
            @endforeach
          </select>
          @error('role')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        @endcan
        <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama..." value="{{ old('name') }}">
          @error('name')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email..." value="{{ old('email') }}">
          @error('email')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password..." value="{{ old('password') }}">
          @error('password')
            <span class="invalid-feedback">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Buat Pengguna</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
