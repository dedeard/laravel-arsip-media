<?php

use App\Http\Controllers\AlbumsController;
use App\Http\Controllers\ShareController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DeletedIncomingLettersController;
use App\Http\Controllers\DeletedOngoingLettersController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IncomingLetterController;
use App\Http\Controllers\LogLettersController;
use App\Http\Controllers\OngoingLetterController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register' => false]);

Route::middleware('auth')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::put('profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::get('profile/logout', [ProfileController::class, 'logout'])->name('profile.logout');

    Route::resource('users', UsersController::class)->except(['show']);

    Route::resource('albums', AlbumsController::class);

    Route::get('albums/share/file/{file_id}', [ShareController::class, 'shareFile'])->name('albums.share.file');
    Route::post('albums/share/file/{file_id}', [ShareController::class, 'createShareFile'])->name('albums.share.file.create');
    Route::delete('albums/share/file/{share_id}', [ShareController::class, 'destroyShareFile'])->name('albums.share.file.destroy');

    Route::get('albums/share/album/{album_id}', [ShareController::class, 'shareAlbum'])->name('albums.share.album');
    Route::post('albums/share/album/{album_id}', [ShareController::class, 'createShareAlbum'])->name('albums.share.album.create');
    Route::delete('albums/share/album/{share_id}', [ShareController::class, 'destroyShareAlbum'])->name('albums.share.album.destroy');

    Route::get('albums/file/{id}', [AlbumsController::class, 'getFile'])->name('albums.file');
    Route::delete('albums/{id}/file/{file_id}', [AlbumsController::class, 'destroyFile'])->name('albums.destroy.file');
    Route::post('albums/{id}', [AlbumsController::class, 'upload'])->name('albums.upload');

    Route::get('roles/toggle-sync-permission/{role_id}/{permission_id}', [RolesController::class, 'toggleSyncPermission'])->name('roles.toggle.sync.permission');
    Route::resource('roles', RolesController::class)->except(['show']);
    // Route::resource('permissions', PermissionsController::class)->except(['show']);
});

Route::get('share/file/{token}', [ShareController::class, 'sharedFile'])->name('share.file');
Route::get('share/album/{token}', [ShareController::class, 'sharedAlbum'])->name('share.album');
Route::get('share/album/{token}/{id}', [ShareController::class, 'fileFromSharedAlbum'])->name('share.album.file');
