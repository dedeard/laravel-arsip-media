<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Image;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class Media extends Model
{
    protected $fillable = [
        'name',
        'original_name',
        'type',
        'size'
    ];

    public function media_able() : MorphTo
    {
        return $this->morphTo();
    }

    public function shares()
    {
        return $this->morphMany(Share::class, 'shareable');
    }

    public function humanSize()
    {
        $size = $this->size;
        if(!$size) {
            return '0.0B';
        }
        $precision = 1;
        $units = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $step = 1024;
        $i = 0;
        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }
        return round($size, $precision).$units[$i];
    }

    public static function createImageThumb($file, $name) {
        try {
            $tmp = Image::make($file)->fit(320, 240, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('public')->put('thumb/' . $name . '.jpeg', (string) $tmp->encode('jpg', 80));
        } catch (\Exception $e){

        }
    }

    public static function createVideoThumb($file, $name) {
        try {
            $contents = FFMpeg::fromDisk('local')
            ->open('albums/' . $name)
            ->getFrameFromSeconds(2)
            ->export()
            ->getFrameContents();
            self::createImageThumb($contents, $name);
        } catch (\Exception $e){
            // dd($e);
        }
    }

    public function destroyFile()
    {
        try {
            Storage::disk('public')->delete('thumb/' . $this->name . '.jpeg');
            Storage::delete('albums/' . $this->name);
        } catch (\Exception $e){
            // dd($e);
        }
    }
}
