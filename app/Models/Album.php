<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Album extends Model
{
    protected $fillable = [
        'title',
        'description',
    ];

    public function media() : MorphMany
    {
        return $this->morphMany(Media::class, 'media_able');
    }

    public function shares()
    {
        return $this->morphMany(Share::class, 'shareable');
    }

    public function humanSize()
    {
        $size = $this->sizes;
        $precision = 1;
        $units = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $step = 1024;
        $i = 0;
        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }
        return round($size, $precision).$units[$i];
    }
}
