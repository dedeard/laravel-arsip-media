<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $fillable = [
        'name',
        'token',
        'expired_at'
    ];

    protected $casts = [
        'expired_at' => 'datetime',
    ];

    public function shareable() : MorphTo
    {
        return $this->morphTo();
    }
}
