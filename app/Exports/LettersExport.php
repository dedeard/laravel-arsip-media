<?php

namespace App\Exports;

use App\Models\Letter;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class LettersExport implements FromCollection, WithStyles
{
    public $data = [
        'incoming' => false,
        'from' => '',
        'to' => '',
    ];

    public function __construct($incoming, $from, $to)
    {
        $this->data = [
            'incoming' => $incoming,
            'from' => $from,
            'to' => $to,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function collection()
    {
        $letters = Letter::where('incoming', $this->data['incoming'])
            ->whereBetween('date', [$this->data['from'], $this->data['to']])
            ->select(['id', 'reference_number', 'subject', 'purpose', 'type', 'date'])
            ->get();
            $letters = $letters->map(function($item, $key){
            $link = $this->data['incoming'] ? route('incoming.show', $item->id)  : route('ongoing.show', $item->id);
            return [
                'reference_number' => $item->reference_number,
                'subject' => $item->subject,
                'purpose' => $item->purpose,
                'type' => $item->type,
                'date' => $item->date->isoFormat('YYYY-MM-DD'),
                'link' => $link
            ];
        });
        $letters->prepend(['reference_number' => 'No Surat', 'subject' => 'Perihal', 'purpose' => 'Tujuan', 'type' => 'Jenis', 'date' => 'Tanggal Masuk', 'link' => 'Link']);
        return $letters;
    }
}
