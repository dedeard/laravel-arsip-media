<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Media;
use App\Models\Share;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ShareController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:share file')->only(['shareFile', 'createShareFile', 'destroyShareFile', 'shareAlbum', 'createShareAlbum', 'destroyShareAlbum']);
    }
    public function shareFile($file_id)
    {
        $media = Media::findOrFail($file_id);
        return view('albums.share', ['media' => $media, 'type' => 'file']);
    }
    public function createShareFile(Request $request, $file_id)
    {
        $media = Media::findOrFail($file_id);
        $data = $request->validate([
            'name' => 'required|string|max:64',
            'hours' => 'nullable|numeric'
        ]);
        $token = Str::random(25);
        $expired_at = null;

        if(isset($data['hours'])){
            $expired_at = now()->addHours($data['hours']);
        }

        $share = new Share([
            'name' => $data['name'],
            'token' => $token,
            'expired_at' => $expired_at
        ]);

        $media->shares()->save($share);
        return redirect()->back()->withSuccess('Link share berhasil dibuat.');
    }
    public function destroyShareFile($share_id)
    {
        Share::findOrFail($share_id)->delete();
        return redirect()->back()->withSuccess('Link share berhasil dihapus.');
    }


    public function shareAlbum($album_id)
    {
        $album = Album::findOrFail($album_id);
        return view('albums.share', ['album' => $album, 'type' => 'album']);
    }
    public function createShareAlbum(Request $request, $album_id)
    {
        $album = Album::findOrFail($album_id);
        $data = $request->validate([
            'name' => 'required|string|max:64',
            'hours' => 'nullable|numeric'
        ]);
        $token = Str::random(25);
        $expired_at = null;

        if(isset($data['hours'])){
            $expired_at = now()->addHours($data['hours']);
        }

        $share = new Share([
            'name' => $data['name'],
            'token' => $token,
            'expired_at' => $expired_at
        ]);

        $album->shares()->save($share);
        return redirect()->back()->withSuccess('Link share berhasil dibuat.');
    }
    public function destroyShareAlbum($share_id)
    {
        Share::findOrFail($share_id)->delete();
        return redirect()->back()->withSuccess('Link share berhasil dihapus.');
    }

    public function sharedFile($token)
    {
        $share = Share::where('token', $token)->firstOrFail();
        abort_if($share->expired_at && $share->expired_at < now(), 404);
        abort_if($share->shareable->title, 404);
        $name = 'albums/' . $share->shareable->name;
        abort_unless(Storage::exists($name), 404);
        return Storage::response($name, $share->shareable->original_name);
    }
    public function sharedAlbum($token)
    {
        $share = Share::where('token', $token)->firstOrFail();
        abort_if($share->expired_at && $share->expired_at < now(), 404);
        abort_if($share->shareable->name, 404);
        abort_unless($share->shareable, 404);
        return view('album', ['album' => $share->shareable, 'token' => $token]);
    }
    public function fileFromSharedAlbum($token, $id)
    {
        $share = Share::where('token', $token)->firstOrFail();
        abort_if($share->expired_at && $share->expired_at < now(), 404);
        abort_if($share->shareable->name, 404);
        abort_unless($share->shareable, 404);
        $media = $share->shareable->media()->findOrFail($id);
        $name = 'albums/' . $media->name;
        abort_unless(Storage::exists($name), 404);
        return Storage::response($name, $media->original_name);
    }
}
