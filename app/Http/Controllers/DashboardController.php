<?php

namespace App\Http\Controllers;
use App\Models\Media;
class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }
}
