<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Media;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class AlbumsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:baca album')->only(['index', 'show']);
        $this->middleware('can:buat album')->only(['create', 'store']);
        $this->middleware('can:edit album')->only(['edit', 'update', 'upload', 'destroyFile', 'getFile']);
        $this->middleware(['can:hapus album', 'password.confirm'])->only('destroy');
    }

    public function index(Request $request)
    {
        if (!empty($request->input('search'))) {
            $search = '%' . $request->input('search') . '%';
            $albums = Album::where('title', 'like', $search);
            $albums->orWhere('description', 'like', $search);
        } else {
            $albums = Album::orderBy('id', 'desc');
        }
        $albums = $albums
            ->withCount(['media', 'media as sizes'=>function($q){
                $q->select(DB::raw('SUM(size)'));
            }])
            ->paginate(20)
            ->appends(['search' => $request->input('search')]);
        return view('albums.index', ['albums' => $albums]);
    }

    public function create()
    {
        return view('albums.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:500']
        ]);
        $album = Album::create($data);
        return redirect()->route('albums.show', $album->id)->withSuccess('Album berhasil dibuat.');
    }

    public function show($id)
    {
        $album = Album::with(['media' => function($q){
            $q->orderBy('created_at', 'desc');
        }])->findOrFail($id);
        return view('albums.show', ['album' => $album]);
    }

    public function edit($id)
    {
        $album = Album::findOrFail($id);
        return view('albums.edit', ['album' => $album]);
    }

    public function update(Request $request, $id)
    {
        $album = Album::findOrFail($id);
        $data = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:500']
        ]);
        $album->update($data);
        return redirect()->back()->withSuccess('Album berhasil diedit.');
    }

    public function destroy($id)
    {
        $album = Album::findOrFail($id);
        foreach($album->media as $media) {
            $media->destroyFile();
            $media->shares()->delete();
            $media->delete();
        }
        $album->shares()->delete();
        $album->delete();
        return redirect()->route('albums.index')->withSuccess('Album telah dihapus');
    }

    public function upload(Request $request, $id)
    {
        $album = Album::findOrFail($id);
        $data = $request->validate(['file' => 'required|mimes:jpeg,jpg,png,flv,mp4,mov,avi|max:10000000']);

        $mime = $data['file']->getMimeType();
        $ext = explode('/', $mime)[1];
        $type = explode('/', $mime)[0];
        $size = $data['file']->getSize();

        $name = $album->id . '/' . now()->timestamp . Str::random(20) . '.' . $ext;
        $data['file']->storeAs('albums', $name);
        $media = new Media([
            'name' => $name,
            'original_name' => $data['file']->getClientOriginalName(),
            'type' => $type,
            'size' => $size,
        ]);
        $album->media()->save($media);
        if($type == 'video') {
            Media::createVideoThumb($data['file'], $name);
        } else {
            Media::createImageThumb($data['file'], $name);
        }
        $album->touch();
    }

    public function getFile($id)
    {
        $media = Media::findOrFail($id);
        $name = 'albums/' . $media->name;
        abort_unless(Storage::exists($name), 404);
        return Storage::response($name, $media->original_name);
    }

    public function destroyFile($id, $file_id)
    {
        $album = Album::findOrFail($id);
        $media = Media::findOrFail($file_id);
        $media->destroyFile();
        $media->shares()->delete();
        $media->delete();
        $album->touch();
        return redirect()->back()->withSuccess('File telah dihapus');
    }
}
