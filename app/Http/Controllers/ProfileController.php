<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile.edit', ['user' => Auth::user()]);
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . Auth::user()->id],
            'new_password' => ['nullable', 'string', 'min:8']
        ]);

        if ($data['new_password']) {
            $data['password'] = Hash::make($data['new_password']);
        } else {
            unset($data['new_password']);
        }

        Auth::user()->update($data);
        return redirect()->back()->withSuccess('Profile teah diedit.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
