<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Rules\RoleExists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:baca pengguna')->only('index');
        $this->middleware('can:buat pengguna')->only(['create', 'store']);
        $this->middleware('can:edit pengguna')->only(['edit', 'update']);
        $this->middleware(['can:hapus pengguna', 'password.confirm'])->only('destroy');
    }

    public function index(Request $request)
    {
        if (!empty($request->input('search'))) {
            $search = '%' . $request->input('search') . '%';
            $users = User::where('name', 'like', $search);
            $users->orWhere('email', 'like', $search);
        } else {
            $users = User::orderBy('id', 'desc');
        }
        $users = $users->paginate(20)->appends(['search' => $request->input('search')]);
        return view('users.index', ['users' => $users]);
    }

    public function create()
    {
        $roles = Role::where('name', '!=', 'Super Admin')->get();
        return view('users.create', ['roles' => $roles]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'role' => ['nullable', 'integer', new RoleExists]
        ]);
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        if(isset($data['role']) && $request->user()->can('edit peran pengguna')) {
            $role = Role::findOrFail($data['role']);
            $user->assignRole($role->name);
        } else {
            $role = Role::findByName('Petugas');
            $user->assignRole($role->name);
        }
        return redirect()->route('users.index')->withSuccess('Pengguna berhasil dibuat.');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('Super Admin') && !Auth::user()->hasRole('Super Admin')) return abort(403);
        $roles = Role::where('name', '!=', 'Super Admin')->get();
        return view('users.edit', ['user' => $user, 'roles' => $roles]);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('Super Admin') && !Auth::user()->hasRole('Super Admin')) return abort(403);
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
            'password' => ['nullable', 'string', 'min:8'],
            'role' => ['nullable', 'integer', new RoleExists]
        ]);
        if ($data['password']) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        $user->update($data);
        if (!$user->hasRole('Super Admin') && $request->user()->can('edit peran pengguna')) {
            if (isset($data['role'])) {
                $role = Role::findOrFail($data['role']);
                $user->syncRoles($role->name);
            } else {
                $user->syncRoles([]);
            }
        }
        return redirect()->back()->withSuccess('Pengguna berhasil diedit.');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('Super Admin')) return abort(403);
        $user->delete();
        return redirect()->route('users.index')->withSuccess('Pengguna telah dihapus.');
    }
}
